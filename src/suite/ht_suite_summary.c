/* ************************************************************************** */
/*                                        _____/\\\\\\\\\___                  */
/*                                         ___/\\\///////\\\_                 */
/*   ht_suite_summary.c                     __\/\\\_____\/\\\__               */
/*                                           __\///\\\\\\\\\/___  |\/| /\ |\ |*/
/*   By: hachiman <adam_bai@protonmail.com>   ___/\\\///////\\\__ |  |/~~\| \|*/
/*                                             __/\\\______\//\\\_            */
/*   Created: 2019/11/23 12:23:27 by hachiman   _\//\\\______/\\\__           */
/*   Updated: 2020/01/06 12:46:49 by hachiman    __\///\\\\\\\\\/___          */
/*                                                ____\/////////______        */
/* ************************************************************************** */

#include "libht.h"

#include <stdio.h>

void	ht_suite_summary(t_ht *ht, t_ht_suite *suite)
{
	printf("%-20s (%d/%d) | ", suite->name, suite->wins, suite->count);
	ht_suite_dots(suite);
	printf("\n");
	if (suite->wins < suite->count && ht->dbg_lvl != HT_DBG_NO)
		ht_suite_messages(ht, suite);
}
