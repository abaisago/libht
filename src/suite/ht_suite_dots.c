/* ************************************************************************** */
/*                                        _____/\\\\\\\\\___                  */
/*                                         ___/\\\///////\\\_                 */
/*   ht_suite_dots.c                        __\/\\\_____\/\\\__               */
/*                                           __\///\\\\\\\\\/___  |\/| /\ |\ |*/
/*   By: hachiman <adam_bai@protonmail.com>   ___/\\\///////\\\__ |  |/~~\| \|*/
/*                                             __/\\\______\//\\\_            */
/*   Created: 2019/11/29 15:52:06 by hachiman   _\//\\\______/\\\__           */
/*   Updated: 2020/01/03 04:00:13 by hachiman    __\///\\\\\\\\\/___          */
/*                                                ____\/////////______        */
/* ************************************************************************** */

#include "libht.h"

#include <stdio.h>

void			ht_suite_dots(t_ht_suite *suite)
{
	int		i;

	i = -1;
	while(++i < suite->count)
		if (suite->units[i]->ran)
			printf("%c", ht_unit_dot(suite->units[i]->exit_status));
		else
			printf("|ERROR NOT RAN|");
}
