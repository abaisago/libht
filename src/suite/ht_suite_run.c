/* ************************************************************************** */
/*                                        _____/\\\\\\\\\___                  */
/*                                         ___/\\\///////\\\_                 */
/*   ht_suite_run.c                         __\/\\\_____\/\\\__               */
/*                                           __\///\\\\\\\\\/___  |\/| /\ |\ |*/
/*   By: hachiman <adam_bai@protonmail.com>   ___/\\\///////\\\__ |  |/~~\| \|*/
/*                                             __/\\\______\//\\\_            */
/*   Created: 2019/11/28 21:13:30 by hachiman   _\//\\\______/\\\__           */
/*   Updated: 2020/01/06 12:03:08 by hachiman    __\///\\\\\\\\\/___          */
/*                                                ____\/////////______        */
/* ************************************************************************** */

#include "libht.h"

void					ht_suite_run(t_ht_suite *suite)
{
	int		i;

	i = -1;
	while (++i < suite->count)
		ht_unit_run(suite->units[i]);
}
