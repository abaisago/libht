/* ************************************************************************** */
/*                                        _____/\\\\\\\\\___                  */
/*                                         ___/\\\///////\\\_                 */
/*   ht_suite_messages.c                    __\/\\\_____\/\\\__               */
/*                                           __\///\\\\\\\\\/___  |\/| /\ |\ |*/
/*   By: hachiman <adam_bai@protonmail.com>   ___/\\\///////\\\__ |  |/~~\| \|*/
/*                                             __/\\\______\//\\\_            */
/*   Created: 2020/01/03 03:56:00 by hachiman   _\//\\\______/\\\__           */
/*   Updated: 2020/01/06 13:05:54 by hachiman    __\///\\\\\\\\\/___          */
/*                                                ____\/////////______        */
/* ************************************************************************** */

#include "libht.h"

#include <stdio.h>

void	ht_suite_messages(t_ht *ht, t_ht_suite *suite)
{
	t_ht_unit	*unit;
	int			i;

	i = -1;
	while(++i < suite->count)
	{
		unit = suite->units[i];
		if (unit->exit_status != 0 && ht->dbg_lvl > HT_DBG_NO)
		{
			printf("%s", unit->msg);
			if (*unit->dbg == HT_DBG_UNIT)
				printf("%s", unit->out->buf);
		}
	}
}
