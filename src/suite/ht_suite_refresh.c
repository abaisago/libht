/* ************************************************************************** */
/*                                        _____/\\\\\\\\\___                  */
/*                                         ___/\\\///////\\\_                 */
/*   ht_suite_refresh.c                     __\/\\\_____\/\\\__               */
/*                                           __\///\\\\\\\\\/___  |\/| /\ |\ |*/
/*   By: hachiman <adam_bai@protonmail.com>   ___/\\\///////\\\__ |  |/~~\| \|*/
/*                                             __/\\\______\//\\\_            */
/*   Created: 2019/11/28 21:08:14 by hachiman   _\//\\\______/\\\__           */
/*   Updated: 2019/12/24 21:21:13 by hachiman    __\///\\\\\\\\\/___          */
/*                                                ____\/////////______        */
/* ************************************************************************** */

#include "libht.h"

#include <sys/wait.h>

void					ht_suite_refresh(t_ht_suite *suite)
{
	int		i;
	int		status;

	i = -1;
	while(++i < suite->count)
	{
		status = suite->units[i]->exit_status;
		if (suite->units[i]->ran)
			if (WIFEXITED(status) && WEXITSTATUS(status) == 0)
				++suite->wins;
	}
}
