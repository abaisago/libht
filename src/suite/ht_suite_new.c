/* ************************************************************************** */
/*                                        _____/\\\\\\\\\___                  */
/*                                         ___/\\\///////\\\_                 */
/*   ht_suite_new.c                         __\/\\\_____\/\\\__               */
/*                                           __\///\\\\\\\\\/___  |\/| /\ |\ |*/
/*   By: hachiman <adam_bai@protonmail.com>   ___/\\\///////\\\__ |  |/~~\| \|*/
/*                                             __/\\\______\//\\\_            */
/*   Created: 2019/11/12 03:33:13 by hachiman   _\//\\\______/\\\__           */
/*   Updated: 2019/12/24 21:20:26 by hachiman    __\///\\\\\\\\\/___          */
/*                                                ____\/////////______        */
/* ************************************************************************** */

#include "libht.h"

#include <stdlib.h>
#include <string.h>

t_ht_suite		*ht_suite_new(char* name)
{
	t_ht_suite	*new_suite;

	if ((new_suite = malloc(sizeof(*new_suite))) == NULL)
		ht_exit("libht: ht_suite_new: struct malloc fail\n");
	if ((new_suite->name = malloc(strlen(name) + 1)) == NULL)
		ht_exit("%lld | libht: ht_suite_new: name malloc fail\n", strlen(name));
	strcpy(new_suite->name, name);
	memset(new_suite->units, 0, sizeof(new_suite->units));
	new_suite->count = 0;
	new_suite->wins = 0;
	new_suite->next = NULL;
	return (new_suite);
}
