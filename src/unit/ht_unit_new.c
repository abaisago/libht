/* ************************************************************************** */
/*                                        _____/\\\\\\\\\___                  */
/*                                         ___/\\\///////\\\_                 */
/*   ht_unit_new.c                          __\/\\\_____\/\\\__               */
/*                                           __\///\\\\\\\\\/___  |\/| /\ |\ |*/
/*   By: hachiman <adam_bai@protonmail.com>   ___/\\\///////\\\__ |  |/~~\| \|*/
/*                                             __/\\\______\//\\\_            */
/*   Created: 2019/11/12 03:16:25 by hachiman   _\//\\\______/\\\__           */
/*   Updated: 2020/01/07 10:22:32 by hachiman    __\///\\\\\\\\\/___          */
/*                                                ____\/////////______        */
/* ************************************************************************** */

#include "libht.h"

#include <errno.h>
#include <string.h>
#include <stdlib.h>

t_ht_unit	*ht_unit_new(char *name, t_ht_fun fun)
{
	t_ht_unit	*new_unit;

	if (name == NULL || fun == NULL)
		ht_exit("libht: ht_unit_new(parameter NULL): %s\n", strerror(errno));
	if ((new_unit = malloc(sizeof(*new_unit))) == NULL)
		ht_exit("libht: ht_unit_new(struct malloc): %s\n", strerror(errno));
	if ((new_unit->name = malloc(strlen(name) + 1)) == NULL)
		ht_exit("libht: ht_unit_new(name malloc): %s\n", strerror(errno));
	strcpy(new_unit->name, name);
	if ((new_unit->msg = ht_shmem_new(HT_SHMEM_SIZE)) == (void*)-1)
		ht_exit("libht: ht_unit_new(shmem_new msg): %s\n", strerror(errno));
	new_unit->msg[0] = '\0';
	if ((new_unit->dbg = ht_shmem_new(sizeof(new_unit->dbg))) == (void*)-1)
		ht_exit("libht: ht_unit_new(shmem_new dbg): %s\n", strerror(errno));
	*new_unit->dbg = HT_DBG_NO;
	new_unit->ran = 0;
	new_unit->fun = fun;
	new_unit->out = ht_string_init();
	new_unit->exit_status = 0;
	return (new_unit);
}
