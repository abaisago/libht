/* ************************************************************************** */
/*                                        _____/\\\\\\\\\___                  */
/*                                         ___/\\\///////\\\_                 */
/*   ht_unit_dot.c                          __\/\\\_____\/\\\__               */
/*                                           __\///\\\\\\\\\/___  |\/| /\ |\ |*/
/*   By: hachiman <adam_bai@protonmail.com>   ___/\\\///////\\\__ |  |/~~\| \|*/
/*                                             __/\\\______\//\\\_            */
/*   Created: 2020/01/03 03:31:22 by hachiman   _\//\\\______/\\\__           */
/*   Updated: 2020/01/03 05:30:10 by hachiman    __\///\\\\\\\\\/___          */
/*                                                ____\/////////______        */
/* ************************************************************************** */

#include "libht.h"

#include <signal.h>
#include <stdlib.h>
#include <sysexits.h>
#include <sys/wait.h>

#include <stdio.h>// // 
static char		exited(int code)
{
	if (code == EXIT_SUCCESS)
		return ('.');
	else if (code == EXIT_FAILURE)
		return ('F');
	else if (code == EX_USAGE)
		return ('U');
	return ('?');
}

static char		signaled(int signal)
{
	if (signal == SIGSEGV)
		return ('S');
	else if (signal == SIGABRT)
		return ('A');
	else if (signal == SIGBUS)
		return ('B');
	else if (signal == SIGKILL)
		return ('K');
	else if (signal == SIGTERM)
		return ('T');
	return ('?');
}

char			ht_unit_dot(int status)
{
	if (WIFEXITED(status))
		return (exited(WEXITSTATUS(status)));
	else if (WIFSIGNALED(status))
		return (signaled(WTERMSIG(status)));
	return ('?');
}
