/* ************************************************************************** */
/*                                        _____/\\\\\\\\\___                  */
/*                                         ___/\\\///////\\\_                 */
/*   ht_unit_run.c                          __\/\\\_____\/\\\__               */
/*                                           __\///\\\\\\\\\/___  |\/| /\ |\ |*/
/*   By: hachiman <adam_bai@protonmail.com>   ___/\\\///////\\\__ |  |/~~\| \|*/
/*                                             __/\\\______\//\\\_            */
/*   Created: 2019/11/25 16:35:05 by hachiman   _\//\\\______/\\\__           */
/*   Updated: 2020/01/21 18:34:45 by hachiman    __\///\\\\\\\\\/___          */
/*                                                ____\/////////______        */
/* ************************************************************************** */

#include "libht.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

static void		child(t_ht_unit *unit, int *fds)
{
	close(fds[0]);
	dup2(fds[1], 1);
	dup2(fds[1], 2);
	unit->fun(unit);
	close(fds[1]);
	exit(unit->exit_status);
}

static void		parent(t_ht_unit *unit, int pid, int *fds)
{
	char	buf[512];
	int		res;

	close(fds[1]);
	waitpid(pid, &(unit->exit_status), 0);
	unit->ran = 1;
	while ((res = read(fds[0], buf, 511)) > 0)
	{
		buf[res] = '\0';
		ht_string_append(unit->out, buf);
	}
	if (res < 0)
		ht_exit("libht: ht_unit_run(read): %s\n", strerror(errno));
	close(fds[0]);
}

void	ht_unit_run(t_ht_unit *unit)
{
	int		fds[2];
	int		pid;

	if ((pipe(fds) == -1) || ((pid = fork()) == -1))
		ht_exit("libht: ht_unit_run(pipe/fork): %s\n", strerror(errno));
	else if (pid == 0)
		child(unit, fds);
	else
		parent(unit, pid, fds);
}
