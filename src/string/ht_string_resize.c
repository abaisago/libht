/* ************************************************************************** */
/*                                        _____/\\\\\\\\\___                  */
/*                                         ___/\\\///////\\\_                 */
/*   ht_string_resize.c                     __\/\\\_____\/\\\__               */
/*                                           __\///\\\\\\\\\/___  |\/| /\ |\ |*/
/*   By: hachiman <adam_bai@protonmail.com>   ___/\\\///////\\\__ |  |/~~\| \|*/
/*                                             __/\\\______\//\\\_            */
/*   Created: 2019/11/28 18:10:36 by hachiman   _\//\\\______/\\\__           */
/*   Updated: 2020/01/22 11:13:17 by hachiman    __\///\\\\\\\\\/___          */
/*                                                ____\/////////______        */
/* ************************************************************************** */

#include "libht.h"

#include <stdint.h>
#include <stdlib.h>

void	ht_string_resize(t_ht_string* string, size_t new_size)
{
	string->buf = (char*)realloc(string->buf, sizeof(char) * new_size);
	if (string->buf == NULL)
		ht_exit("libht: string_resize(realloc buf): %s\n", strerror(errno));
	string->size = new_size;
}
