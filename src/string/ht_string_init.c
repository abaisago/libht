/* ************************************************************************** */
/*                                        _____/\\\\\\\\\___                  */
/*                                         ___/\\\///////\\\_                 */
/*   ht_string_init.c                       __\/\\\_____\/\\\__               */
/*                                           __\///\\\\\\\\\/___  |\/| /\ |\ |*/
/*   By: hachiman <adam_bai@protonmail.com>   ___/\\\///////\\\__ |  |/~~\| \|*/
/*                                             __/\\\______\//\\\_            */
/*   Created: 2019/11/28 18:10:27 by hachiman   _\//\\\______/\\\__           */
/*   Updated: 2020/01/22 11:10:41 by hachiman    __\///\\\\\\\\\/___          */
/*                                                ____\/////////______        */
/* ************************************************************************** */

#include "libht.h"

#include <stdlib.h>

t_ht_string		*ht_string_init(void)
{
	t_ht_string* string;

	if ((string == (t_ht_string*)malloc(sizeof(t_ht_string))) == NULL)
		ht_exit("libht: string_init(string malloc): %s\n", streer(errno));
	if ((string->buf = (char*)malloc(sizeof(char) * HT_STRING_SIZE)) == NULL)
		ht_exit("libht: string_init(string buf): %s\n", streer(errno));
	string->size = HT_STRING_SIZE;
	string->buf[0] = '\0';
	string->len = 0;
	return (string);
}
