/* ************************************************************************** */
/*                                        _____/\\\\\\\\\___                  */
/*                                         ___/\\\///////\\\_                 */
/*   ht_string_append.c                     __\/\\\_____\/\\\__               */
/*                                           __\///\\\\\\\\\/___  |\/| /\ |\ |*/
/*   By: hachiman <adam_bai@protonmail.com>   ___/\\\///////\\\__ |  |/~~\| \|*/
/*                                             __/\\\______\//\\\_            */
/*   Created: 2019/11/28 18:10:02 by hachiman   _\//\\\______/\\\__           */
/*   Updated: 2020/01/22 11:15:37 by hachiman    __\///\\\\\\\\\/___          */
/*                                                ____\/////////______        */
/* ************************************************************************** */

#include "libht.h"

#include <string.h>

void	ht_string_append(t_ht_string* string, const char *text)
{
	size_t		len;
	size_t		new_len;
	unsigned	mult;

	if (text == NULL)
		text = "(null)";
	len = strlen(text);
	new_len = string->len + len + 1;
	if (new_len > string->size)
	{
		mult = (new_len - string->size) / HT_STRING_INC + 1;
		ht_string_resize(string, string->size + HT_STRING_INC * mult);
	}
	string->len += len;
	strcat(string->buf, text);
}
