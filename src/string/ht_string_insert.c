/* ************************************************************************** */
/*                                        _____/\\\\\\\\\___                  */
/*                                         ___/\\\///////\\\_                 */
/*   ht_string_insert.c                     __\/\\\_____\/\\\__               */
/*                                           __\///\\\\\\\\\/___  |\/| /\ |\ |*/
/*   By: hachiman <adam_bai@protonmail.com>   ___/\\\///////\\\__ |  |/~~\| \|*/
/*                                             __/\\\______\//\\\_            */
/*   Created: 2019/12/24 22:24:52 by hachiman   _\//\\\______/\\\__           */
/*   Updated: 2020/01/03 02:06:22 by hachiman    __\///\\\\\\\\\/___          */
/*                                                ____\/////////______        */
/* ************************************************************************** */

#include "libht.h"

#include <stddef.h>
#include <string.h>

void	ht_string_insert(t_ht_string* string, const char *text, size_t pos)
{
	size_t		len;
	size_t		new_len;
	unsigned	mult;

	len = strlen(text);
	if (pos > string->len)
		pos = string->len;
	new_len = string->len + len + 1;
	if (new_len > string->size)
	{
		mult = (new_len - string->size) / HT_STRING_INC + 1;
		ht_string_resize(string, string->size + HT_STRING_INC * mult);
	}
	memmove(string->buf + pos + len, string->buf + pos, string->len - pos + 1);
	string->len += len;
	memcpy(string->buf + pos, text, len);
	string->buf[string->len] = '\0';
}
