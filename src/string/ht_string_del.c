/* ************************************************************************** */
/*                                        _____/\\\\\\\\\___                  */
/*                                         ___/\\\///////\\\_                 */
/*   ht_string_del.c                        __\/\\\_____\/\\\__               */
/*                                           __\///\\\\\\\\\/___  |\/| /\ |\ |*/
/*   By: hachiman <adam_bai@protonmail.com>   ___/\\\///////\\\__ |  |/~~\| \|*/
/*                                             __/\\\______\//\\\_            */
/*   Created: 2019/11/28 18:10:16 by hachiman   _\//\\\______/\\\__           */
/*   Updated: 2019/11/28 18:10:17 by hachiman    __\///\\\\\\\\\/___          */
/*                                                ____\/////////______        */
/* ************************************************************************** */

#include "libht.h"

#include <stdlib.h>
#include <string.h>

void	ht_string_del(t_ht_string** address)
{
	t_ht_string		*string = *address;

	memset(string->buf, 0, string->size);
	string->len = 0;
	free(string->buf);
	string->buf = NULL;
	string->size = 0;
	*address = NULL;
}
