/* ************************************************************************** */
/*                                        _____/\\\\\\\\\___                  */
/*                                         ___/\\\///////\\\_                 */
/*   ht_add_suite.c                         __\/\\\_____\/\\\__               */
/*                                           __\///\\\\\\\\\/___  |\/| /\ |\ |*/
/*   By: hachiman <adam_bai@protonmail.com>   ___/\\\///////\\\__ |  |/~~\| \|*/
/*                                             __/\\\______\//\\\_            */
/*   Created: 2019/11/23 16:05:06 by hachiman   _\//\\\______/\\\__           */
/*   Updated: 2019/11/28 21:25:54 by hachiman    __\///\\\\\\\\\/___          */
/*                                                ____\/////////______        */
/* ************************************************************************** */

#include "libht.h"

#include <stdlib.h>

void	ht_add_suite(t_ht *ht, t_ht_suite *suite)
{
	t_ht_cat	*cat;

	cat = ht->last;
	if (cat->suites == NULL)
	{
		cat->suites = suite;
		cat->last = suite;
	}
	else
	{
		cat->last->next = suite;
		cat->last = suite;
	}
}
