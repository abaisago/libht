/* ************************************************************************** */
/*                                        _____/\\\\\\\\\___                  */
/*                                         ___/\\\///////\\\_                 */
/*   ht_new.c                               __\/\\\_____\/\\\__               */
/*                                           __\///\\\\\\\\\/___  |\/| /\ |\ |*/
/*   By: hachiman <adam_bai@protonmail.com>   ___/\\\///////\\\__ |  |/~~\| \|*/
/*                                             __/\\\______\//\\\_            */
/*   Created: 2019/11/12 04:15:21 by hachiman   _\//\\\______/\\\__           */
/*   Updated: 2020/01/04 07:07:14 by hachiman    __\///\\\\\\\\\/___          */
/*                                                ____\/////////______        */
/* ************************************************************************** */

#include "libht.h"

#include <stdlib.h>
#include <string.h>

t_ht			*ht_new(char* name)
{
	t_ht	*new_test;

	if ((new_test = malloc(sizeof(*new_test))) == NULL)
		ht_exit("libht: ht_new: struct malloc fail\n");
	if ((new_test->name = malloc(sizeof(*name) * (strlen(name) + 1))) == NULL)
		ht_exit("libht: ht_new: name malloc fail\n");
	strcpy(new_test->name, name);
	new_test->cats = NULL;
	new_test->last = NULL;
	new_test->count = 0;
	new_test->wins = 0;
	new_test->dbg_lvl = 0;
	return (new_test);
}
