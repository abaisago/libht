/* ************************************************************************** */
/*                                        _____/\\\\\\\\\___                  */
/*                                         ___/\\\///////\\\_                 */
/*   ht_add_unit.c                          __\/\\\_____\/\\\__               */
/*                                           __\///\\\\\\\\\/___  |\/| /\ |\ |*/
/*   By: hachiman <adam_bai@protonmail.com>   ___/\\\///////\\\__ |  |/~~\| \|*/
/*                                             __/\\\______\//\\\_            */
/*   Created: 2019/11/12 04:35:46 by hachiman   _\//\\\______/\\\__           */
/*   Updated: 2019/12/24 21:18:01 by hachiman    __\///\\\\\\\\\/___          */
/*                                                ____\/////////______        */
/* ************************************************************************** */

#include "libht.h"

void	ht_add_unit(t_ht *ht, t_ht_unit *unit)
{
	t_ht_suite	*suite;

	suite = ht->last->last;
	if (suite->count == HT_MAX_UNITS)
		ht_exit("libht: Max units count reached\n");
	suite->units[suite->count] = unit;
	suite->count++;
}
