/* ************************************************************************** */
/*                                        _____/\\\\\\\\\___                  */
/*                                         ___/\\\///////\\\_                 */
/*   ht_argv_parser.c                       __\/\\\_____\/\\\__               */
/*                                           __\///\\\\\\\\\/___  |\/| /\ |\ |*/
/*   By: hachiman <adam_bai@protonmail.com>   ___/\\\///////\\\__ |  |/~~\| \|*/
/*                                             __/\\\______\//\\\_            */
/*   Created: 2020/01/07 11:34:35 by hachiman   _\//\\\______/\\\__           */
/*   Updated: 2020/01/07 14:08:16 by hachiman    __\///\\\\\\\\\/___          */
/*                                                ____\/////////______        */
/* ************************************************************************** */

#include "libht.h"

#include <errno.h>
#include <stdlib.h>
#include <string.h>

typedef struct		s_list
{
	void			*data;
	struct s_list	*next;
}					t_list;

static t_list		*list_new(void	*data)
{
	t_list	*new;

	if ((new = malloc(sizeof(*new))) == NULL)
		ht_exit("libht: list_new(malloc new): %s\n", strerror(errno));
	new->data = data;
	new->next = NULL;
	return (new);
}

char				**list2argv(t_list *head, int size)
{
	char	**argv;
	int		i;

	if ((argv = malloc(sizeof(*argv) * (size + 1))) == NULL)
		ht_exit("libht: list2argv(malloc argv): %s\n", strerror(errno));
	for (i = 0; i < size; ++i)
	{
		argv[i] = head->data;
		head = head->next;
	}
	return (argv);
}

void				list_del(t_list *head)
{
	t_list	*prev;

	while (head != NULL)
	{
		prev = head;
		head = head->next;
		free(prev);
	}
	prev = NULL;
}

char			**ht_argv_parser(char *str)
{
	t_list	*head;
	t_list	*list;
	int		size;
	char    *out;
	char	*delim;
	char	**argv;

	list = NULL;
	size = 0;
	delim = " ";
	out = strtok(str, delim);
	while (out != NULL)
	{
		if (list == NULL)
		{
			list = list_new(out);
			head = list;
		}
		else
		{
			list->next = list_new(out);
			list = list->next;
		}
		++size;
		out = strtok(NULL, delim);
	}
	argv = list2argv(head, size);
	list_del(head);
	return (argv);
}
