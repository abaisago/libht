/* ************************************************************************** */
/*                                        _____/\\\\\\\\\___                  */
/*                                         ___/\\\///////\\\_                 */
/*   ht_itoa.c                              __\/\\\_____\/\\\__               */
/*                                           __\///\\\\\\\\\/___  |\/| /\ |\ |*/
/*   By: hachiman <adam_bai@protonmail.com>   ___/\\\///////\\\__ |  |/~~\| \|*/
/*                                             __/\\\______\//\\\_            */
/*   Created: 2020/01/03 03:01:36 by hachiman   _\//\\\______/\\\__           */
/*   Updated: 2020/01/03 03:21:37 by hachiman    __\///\\\\\\\\\/___          */
/*                                                ____\/////////______        */
/* ************************************************************************** */

#include "libht.h"

#include <stddef.h>
#include <stdlib.h>
#include <string.h>

/*
** Counts the number of digits and the
** negative sign of any signed integer.
*/

static size_t	count_digits(intmax_t n, uint8_t base)
{
	size_t	cnt;

	if (n == 0)
		return (1);
	cnt = 0;
	if (n < 0)
		cnt = 1;
	while (n != 0)
	{
		n /= base;
		++cnt;
	}
	return (cnt);
}

static void		fill_string(intmax_t nbr, char *str, size_t *i)
{
	if (nbr >= 10)
	{
		fill_string(nbr / 10, str, i);
		fill_string(nbr % 10, str, i);
	}
	else
	{
		str[*i] = nbr + '0';
		(*i)++;
	}
}

/*
** Changes an integer to it's absolute value and
** outputs 1 if it has changed, 0 otherwise.
*/

static int		ht_abs(intmax_t *n)
{
	if (*n < 0)
	{
		*n = -(*n);
		return (1);
	}
	return (0);
}

/*
** Takes an int and outpus it's allocated string representation.
*/

char			*ht_itoa(intmax_t n)
{
	size_t		i;
	char		*res;

	i = 0;
	res = (char*)malloc(sizeof(*res) * (count_digits(n, 10) + 1));
	if (res == NULL)
		return (NULL);
	if (ht_abs(&n))
		res[i++] = '-';
	fill_string(n, res, &i);
	return (res);
}
