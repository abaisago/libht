/* ************************************************************************** */
/*                                        _____/\\\\\\\\\___                  */
/*                                         ___/\\\///////\\\_                 */
/*   ht_cat_refresh.c                       __\/\\\_____\/\\\__               */
/*                                           __\///\\\\\\\\\/___  |\/| /\ |\ |*/
/*   By: hachiman <adam_bai@protonmail.com>   ___/\\\///////\\\__ |  |/~~\| \|*/
/*                                             __/\\\______\//\\\_            */
/*   Created: 2019/11/28 21:34:44 by hachiman   _\//\\\______/\\\__           */
/*   Updated: 2019/11/28 21:41:30 by hachiman    __\///\\\\\\\\\/___          */
/*                                                ____\/////////______        */
/* ************************************************************************** */

#include "libht.h"


void	ht_cat_refresh(t_ht_cat *cat)
{
	t_ht_suite	*suite;

	suite = cat->suites;
	while (suite != NULL)
	{
		ht_suite_refresh(suite);
		cat->count += suite->count;
		cat->wins += suite->wins;
		suite = suite->next;
	}
}
