/* ************************************************************************** */
/*                                        _____/\\\\\\\\\___                  */
/*                                         ___/\\\///////\\\_                 */
/*   ht_cat_summary.c                       __\/\\\_____\/\\\__               */
/*                                           __\///\\\\\\\\\/___  |\/| /\ |\ |*/
/*   By: hachiman <adam_bai@protonmail.com>   ___/\\\///////\\\__ |  |/~~\| \|*/
/*                                             __/\\\______\//\\\_            */
/*   Created: 2019/11/23 16:11:05 by hachiman   _\//\\\______/\\\__           */
/*   Updated: 2020/01/04 07:05:17 by hachiman    __\///\\\\\\\\\/___          */
/*                                                ____\/////////______        */
/* ************************************************************************** */

#include "libht.h"

#include <stdio.h>

void	ht_cat_summary(t_ht *ht, t_ht_cat *cat)
{
	t_ht_suite	*suite;

	printf("-> %-16s (%d/%d)\n", cat->name, cat->wins, cat->count);
	suite = cat->suites;
	while (suite != NULL)
	{
		ht_suite_summary(ht, suite);
		suite = suite->next;
	}
}
