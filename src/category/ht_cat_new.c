/* ************************************************************************** */
/*                                        _____/\\\\\\\\\___                  */
/*                                         ___/\\\///////\\\_                 */
/*   ht_cat_new.c                           __\/\\\_____\/\\\__               */
/*                                           __\///\\\\\\\\\/___  |\/| /\ |\ |*/
/*   By: hachiman <adam_bai@protonmail.com>   ___/\\\///////\\\__ |  |/~~\| \|*/
/*                                             __/\\\______\//\\\_            */
/*   Created: 2019/11/23 14:33:43 by hachiman   _\//\\\______/\\\__           */
/*   Updated: 2019/11/28 18:09:34 by hachiman    __\///\\\\\\\\\/___          */
/*                                                ____\/////////______        */
/* ************************************************************************** */

#include "libht.h"

#include <stdlib.h>
#include <string.h>

t_ht_cat	*ht_cat_new(char* name)
{
	t_ht_cat	*new_cat;

	if ((new_cat = malloc(sizeof(*new_cat))) == NULL)
		ht_exit("libht: ht_suite_new: struct malloc fail\n");
	if ((new_cat->name = malloc(strlen(name) + 1)) == NULL)
		ht_exit("%lld | libht: ht_suite_new: name malloc fail\n", strlen(name));
	strcpy(new_cat->name, name);
	new_cat->suites = NULL;
	new_cat->last = NULL;
	new_cat->count = 0;
	new_cat->wins = 0;
	new_cat->next = NULL;
	return (new_cat);
}
