/* ************************************************************************** */
/*                                        _____/\\\\\\\\\___                  */
/*                                         ___/\\\///////\\\_                 */
/*   ht_internal_strequ.c                   __\/\\\_____\/\\\__               */
/*                                           __\///\\\\\\\\\/___  |\/| /\ |\ |*/
/*   By: hachiman <adam_bai@protonmail.com>   ___/\\\///////\\\__ |  |/~~\| \|*/
/*                                             __/\\\______\//\\\_            */
/*   Created: 2020/01/06 11:36:24 by hachiman   _\//\\\______/\\\__           */
/*   Updated: 2020/01/06 13:26:59 by hachiman    __\///\\\\\\\\\/___          */
/*                                                ____\/////////______        */
/* ************************************************************************** */

#include "libht.h"

#include <stdio.h>
#include <string.h>

void	ht_internal_strequ(t_ht_unit *unit, t_ht_filepos *pos,
			char* message, char* actual, char *expected)
{
	int		condition;

	condition = !strcmp(actual, expected);
	if (!condition)
	{
		printf("----|actual   <%s>\n", actual);
		printf("----|expected <%s>\n", expected);
		ht_internal_fail(unit, pos, message);
	}
}
