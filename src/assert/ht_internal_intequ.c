/* ************************************************************************** */
/*                                        _____/\\\\\\\\\___                  */
/*                                         ___/\\\///////\\\_                 */
/*   ht_internal_intequ.c                   __\/\\\_____\/\\\__               */
/*                                           __\///\\\\\\\\\/___  |\/| /\ |\ |*/
/*   By: hachiman <adam_bai@protonmail.com>   ___/\\\///////\\\__ |  |/~~\| \|*/
/*                                             __/\\\______\//\\\_            */
/*   Created: 2020/01/07 20:00:25 by hachiman   _\//\\\______/\\\__           */
/*   Updated: 2020/01/07 21:13:02 by hachiman    __\///\\\\\\\\\/___          */
/*                                                ____\/////////______        */
/* ************************************************************************** */

#include "libht.h"

#include <stddef.h>
#include <stdio.h>

void	ht_internal_intequ(t_ht_unit *unit, t_ht_filepos *pos,
			char* message, intmax_t actual,  intmax_t expected)
{
	int		condition;

	condition = actual - expected;
	if (!condition)
	{
		printf("----|actual   <%jd>\n", actual);
		printf("----|expected <%jd>\n", expected);
		ht_internal_fail(unit, pos, message);
	}
}
