/* ************************************************************************** */
/*                                        _____/\\\\\\\\\___                  */
/*                                         ___/\\\///////\\\_                 */
/*   ht_internal_floatnequ.c                __\/\\\_____\/\\\__               */
/*                                           __\///\\\\\\\\\/___  |\/| /\ |\ |*/
/*   By: hachiman <adam_bai@protonmail.com>   ___/\\\///////\\\__ |  |/~~\| \|*/
/*                                             __/\\\______\//\\\_            */
/*   Created: 2020/01/07 20:31:34 by hachiman   _\//\\\______/\\\__           */
/*   Updated: 2020/01/07 20:34:10 by hachiman    __\///\\\\\\\\\/___          */
/*                                                ____\/////////______        */
/* ************************************************************************** */

#include "libht.h"

#include <stddef.h>
#include <stdio.h>

void	ht_internal_floatnequ(t_ht_unit *unit, t_ht_filepos *pos,
			char* message, long double actual,
			long double expected, long double delta)
{
	int		condition;

	condition = actual - expected;
	if (condition > (0 - delta) && condition < (0 + delta))
	{
		printf("----|actual   <%Lf>\n", actual);
		printf("----|expected <%Lf>\n", expected);
		ht_internal_fail(unit, pos, message);
	}
}
