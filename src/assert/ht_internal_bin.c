/* ************************************************************************** */
/*                                        _____/\\\\\\\\\___                  */
/*                                         ___/\\\///////\\\_                 */
/*   ht_internal_bin.c                      __\/\\\_____\/\\\__               */
/*                                           __\///\\\\\\\\\/___  |\/| /\ |\ |*/
/*   By: hachiman <adam_bai@protonmail.com>   ___/\\\///////\\\__ |  |/~~\| \|*/
/*                                             __/\\\______\//\\\_            */
/*   Created: 2020/01/07 11:23:53 by hachiman   _\//\\\______/\\\__           */
/*   Updated: 2020/01/07 20:05:19 by hachiman    __\///\\\\\\\\\/___          */
/*                                                ____\/////////______        */
/* ************************************************************************** */

#include "libht.h"

#include <errno.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

static char			*make_argv_line(char *bin, char * args)
{
	char	*argv;
	int		len;

	len = strlen(bin) + strlen(args);
	if ((argv = malloc(len + 2)) == NULL)
		ht_exit("libht: internal_bin make_argv(malloc argv):%s\n", strerror(errno));
	argv[len] = '\0';
	strcpy(argv, bin);
	strcat(argv, " ");
	strcat(argv, args);
	return (argv);
}

static void			child(char *argv_line, int *fds)
{
	extern char	**environ;
	char		**argv;

	close(fds[0]);
	dup2(fds[1], 1);
	dup2(fds[1], 2);
	argv = ht_argv_parser(argv_line);
	close(fds[1]);
	if ((execve(argv[0], argv, environ)) == -1)
		ht_exit("libht: internal_bin run(execve): %s: %s\n",
				argv[0], strerror(errno));
}

static t_ht_string	*parent(int pid, int *fds)
{
	/* unit->out, &exit->status */
	char			buf[512];
	int				res;
	t_ht_string		*out;

	out = ht_string_init();
	close(fds[1]);
	waitpid(pid, NULL, 0);
	while ((res = read(fds[0], buf, 511)) > 0)
	{
		buf[res] = '\0';
		ht_string_append(out, buf);
	}
	if (res < 0)
		ht_exit("libht: ht_unit_run(read): %s\n", strerror(errno));
	close(fds[0]);
	return (out);
}

static t_ht_string	*run(char *bin, char *args, int *fds)
{
	char	*argv_line;
	int		pid;

	argv_line = make_argv_line(bin, args);
	if ((pid = fork()) == -1)
		ht_exit("libht: ht_unit_run(pipe/fork): %s\n", strerror(errno));
	if (pid == 0)
		child(argv_line, fds);
	return (parent(pid, fds));
}

void				ht_internal_bin(t_ht_unit *unit, t_ht_filepos *pos,
						char* message, char	*actual, char *actual_args,
						char *expected, char *expected_args)
{
	int			condition;
	int			fd_ac[2];
	int			fd_ex[2];
	t_ht_string	*ac;
	t_ht_string	*ex;

	if (pipe(fd_ac) == -1 || pipe(fd_ex) == -1)
		ht_exit("libht: internal_bin(pipes): %s\n", strerror(errno));
	ac = run(actual, actual_args, fd_ac);
	ex = run(expected, expected_args, fd_ex);
	condition = strcmp(ac->buf, ex->buf);
	if (condition)
	{
		printf("------------------------------------ ");
		printf("actual -------------------------------------\n");
		printf("%s", ac->buf);
		printf("----------------------------------- ");
		printf("expected ------------------------------------\n");
		printf("%s", ex->buf);
		ht_internal_fail(unit, pos, message);
	}
}
