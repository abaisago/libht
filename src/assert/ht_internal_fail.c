/* ************************************************************************** */
/*                                        _____/\\\\\\\\\___                  */
/*                                         ___/\\\///////\\\_                 */
/*   ht_internal_fail.c                     __\/\\\_____\/\\\__               */
/*                                           __\///\\\\\\\\\/___  |\/| /\ |\ |*/
/*   By: hachiman <adam_bai@protonmail.com>   ___/\\\///////\\\__ |  |/~~\| \|*/
/*                                             __/\\\______\//\\\_            */
/*   Created: 2019/11/26 21:29:38 by hachiman   _\//\\\______/\\\__           */
/*   Updated: 2020/01/06 12:45:43 by hachiman    __\///\\\\\\\\\/___          */
/*                                                ____\/////////______        */
/* ************************************************************************** */

#include "libht.h"

#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

void	ht_internal_fail(t_ht_unit *unit, t_ht_filepos *fpos, char *message)
{
	int		res;
	int		rest;

	res = snprintf(unit->msg, HT_SHMEM_SIZE,
		"-> %s:%d:%s\n", fpos->file, fpos->line, unit->name);
	rest = HT_SHMEM_SIZE - res;
	if (message != NULL && rest > 0)
		snprintf(unit->msg + res - 1, rest, ": %s\n", message);
	unit->exit_status = 1;
}
