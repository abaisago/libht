/* ************************************************************************** */
/*                                        _____/\\\\\\\\\___                  */
/*                                         ___/\\\///////\\\_                 */
/*   libht.h                                __\/\\\_____\/\\\__               */
/*                                           __\///\\\\\\\\\/___  |\/| /\ |\ |*/
/*   By: hachiman <adam_bai@protonmail.com>   ___/\\\///////\\\__ |  |/~~\| \|*/
/*                                             __/\\\______\//\\\_            */
/*   Created: 2019/11/12 01:54:40 by hachiman   _\//\\\______/\\\__           */
/*   Updated: 2020/01/07 21:11:36 by hachiman    __\///\\\\\\\\\/___          */
/*                                                ____\/////////______        */
/* ************************************************************************** */

#ifndef LIBHT_H
# define LIBHT_H
# include <stddef.h>
# include <stdint.h>

# define HT_DBG_NO 0
# define HT_DBG_UNIT 1
# define HT_DBG_LINE 1
# define HT_DBG_FULL 2 // // unused ?
# define HT_MAX_UNITS 32
# define HT_STRING_SIZE 256
# define HT_STRING_INC	256
# define HT_SHMEM_SIZE	2048

typedef struct 			s_ht_unit	t_ht_unit;
typedef struct 			s_ht_cat	t_ht_cat;
typedef struct 			s_ht		t_ht;

/*
**************************************************************************
**                             HT UTIL                                  **
**************************************************************************
*/

typedef struct			s_ht_filepos
{
	const char			*file;
	int					line;
}						t_ht_filepos;

char					**ht_argv_parser(char *str); // // make better
void					ht_exit(const char *fmt, ...);
char					*ht_itoa(intmax_t n);
void   					*ht_shmem_new(size_t size);

/*
**************************************************************************
**                            HT_STRING                                 **
**************************************************************************
*/

typedef struct			s_ht_string
{
	char				*buf;
	size_t				len;
	size_t				size;
}						t_ht_string;

void					ht_string_append(t_ht_string* string, const char *text);
void					ht_string_del(t_ht_string** address);
void					ht_string_insert(t_ht_string* string, const char *text,
							size_t pos);
t_ht_string				*ht_string_init(void);
void					ht_string_resize(t_ht_string* string, size_t new_size);

/*
**************************************************************************
**                             HT UNIT                                  **
**************************************************************************
*/

typedef	void (*t_ht_fun)(t_ht_unit*);

typedef struct			s_ht_unit
{
	char				*name;
	char				*msg;
	int8_t				*dbg;
	int8_t				ran;
	t_ht_fun			fun;
	t_ht_string			*out;
	int					exit_status;
}						t_ht_unit;

t_ht_unit				*ht_unit_new(char* name, t_ht_fun fun);
char					ht_unit_dot(int status);
void					ht_unit_run(t_ht_unit *unit);

/*
**************************************************************************
**                             HT SUITE                                 **
**************************************************************************
*/

typedef struct			s_ht_suite
{
	char				*name;
	t_ht_unit			*units[HT_MAX_UNITS];
	int					count;
	int					wins;
	struct s_ht_suite	*next;
}						t_ht_suite;

void					ht_suite_dots(t_ht_suite *suite);
void					ht_suite_messages(t_ht *ht, t_ht_suite *suite);
t_ht_suite				*ht_suite_new(char* name);
void					ht_suite_refresh(t_ht_suite *suite);
void					ht_suite_run(t_ht_suite *suite);
void					ht_suite_summary(t_ht *ht, t_ht_suite *suite);

/*
**************************************************************************
**                          HT CATEGORY                                 **
**************************************************************************
*/

typedef struct			s_ht_cat
{
	char				*name;
	t_ht_suite			*suites;
	t_ht_suite			*last;
	int					count;
	int					wins;
	struct s_ht_cat		*next;
}						t_ht_cat;

t_ht_cat				*ht_cat_new(char* name);
void					ht_cat_refresh(t_ht_cat *cat);
void					ht_cat_run(t_ht_cat *cat);
void					ht_cat_summary(t_ht *ht, t_ht_cat *cat);

/*
**************************************************************************
**                                HT                                    **
**************************************************************************
*/

typedef struct			s_ht
{
	char				*name;
	t_ht_cat			*cats;
	t_ht_cat			*last;
	int					count;
	int					wins;
	unsigned char		dbg_lvl;
}						t_ht;

void					ht_add_unit(t_ht *ht, t_ht_unit *unit);
void					ht_add_suite(t_ht *ht, t_ht_suite *suite);
void					ht_add_cat(t_ht *ht, t_ht_cat *cat);
t_ht					*ht_new(char* name);
void					ht_refresh(t_ht *ht);
void					ht_run(t_ht *ht);
void					ht_dbg_lvl_set(t_ht *ht, unsigned char lvl);
void					ht_summary(t_ht *ht);
int						ht_won(t_ht *ht);

/*
**************************************************************************
**                         HT ASSERTS & MACROS                          **
**************************************************************************
*/

/*
** Internal Asserts
*/

void					ht_internal_fail(t_ht_unit *unit, t_ht_filepos *fpos,
							char *message);
void					ht_internal(t_ht_unit *unit, t_ht_filepos *fpos,
							char *message, int condition);


void					ht_internal_floatequ(t_ht_unit *unit, t_ht_filepos *pos,
							char* message, long double actual,
							long double expected, long double delta);
void					ht_internal_floatnequ(t_ht_unit *unit, t_ht_filepos *pos,
							char* message, long double actual,
							long double expected, long double delta);
void					ht_internal_intequ(t_ht_unit *unit, t_ht_filepos *pos,
							char* message, intmax_t actual,  intmax_t expected);
void					ht_internal_intnequ(t_ht_unit *unit, t_ht_filepos *pos,
							char* message, intmax_t actual,  intmax_t expected);
void					ht_internal_strequ(t_ht_unit *unit, t_ht_filepos *pos,
							char* message, char *actual, char *expected);
void					ht_internal_strnequ(t_ht_unit *unit, t_ht_filepos *pos,
							char* message, char *actual, char *expected);

void					ht_internal_bin(t_ht_unit *unit, t_ht_filepos *pos,
							char* message, char	*actual, char *actual_args,
							char *expected, char *expected_args);

/*
** Public Asserts
*/

#define FILEPOS (&(t_ht_filepos){__FILE__, __LINE__})

#define ht_assert(cond) ht_internal(unit, FILEPOS, NULL,(cond))
#define ht_assert_msg(ms, cond) ht_internal(unit, FILEPOS, (ms), (cond))

/* float */
#define ht_assert_floatequ(ac, ex) ht_internal_floatequ(unit, FILEPOS, NULL, (ac), (ex), 0)
#define ht_assert_floatequ_msg(ms, ac, ex) ht_internal_floatequ(unit, FILEPOS, (ms), (ac), (ex), 0)
#define ht_assert_floatnequ(ac, ex) ht_internal_floatnequ(unit, FILEPOS, NULL, (ac), (ex), 0)
#define ht_assert_floatnequ_msg(ms, ac, ex) ht_internal_floatnequ(unit, FILEPOS, (ms), (ac), (ex), 0)

/* int */
#define ht_assert_intequ(ac, ex) ht_internal_intequ(unit, FILEPOS, NULL, (ac), (ex))
#define ht_assert_intequ_msg(ms, ac, ex) ht_internal_intequ(unit, FILEPOS, (ms), (ac), (ex))
#define ht_assert_intnequ(ac, ex) ht_internal_intnequ(unit, FILEPOS, NULL, (ac), (ex))
#define ht_assert_intnequ_msg(ms, ac, ex) ht_internal_intnequ(unit, FILEPOS, (ms), (ac), (ex))

/* string */
#define ht_assert_strequ(ac, ex) ht_internal_strequ(unit, FILEPOS, NULL, (ac), (ex))
#define ht_assert_strequ_msg(ms, ac, ex) ht_internal_strequ(unit, FILEPOS, (ms), (ac), (ex))
#define ht_assert_strnequ(ac, ex) ht_internal_strnequ(unit, FILEPOS, NULL, (ac), (ex))
#define ht_assert_strnequ_msg(ms, ac, ex) ht_internal_strnequ(unit, FILEPOS, (ms), (ac), (ex))

#define ht_assert_bin(ac, acarg, ex, exarg) ht_internal_bin(unit, FILEPOS, NULL, (ac), (acarg), (ex), (exarg))
#define ht_assert_bin_msg(ms, ac, acarg, ex, exarg) ht_internal_bin(unit, FILEPOS, (ms), (ac), (acarg), (ex), (exarg))


/*
** Macros
*/

#define HTEST(VAR, NAME) t_ht	*VAR = ht_new(NAME);
#define CATEGORY(HT, NAME) ht_add_cat(HT, ht_cat_new(NAME))
#define SUITE(HT, NAME) ht_add_suite(HT, ht_suite_new(NAME))
#define ADD_UNIT(HT, F) ht_add_unit(HT, ht_unit_new(#F, F))
#define RUN(HT) ht_run(HT)
#define DBG(HT) (HT->dbg_lvl = HT_DBG_LINE)
#define SUMMARY(HT) ht_summary(HT)
#define WON(HT) ht_won(HT)

#define TEST_FUNCTION(NAME) void NAME(t_ht_unit *unit)
#define	UNIT t_ht_unit *unit
#define DBG_UNIT() (*unit->dbg = HT_DBG_UNIT)

/*
** LIBHT_H
*/
#endif
