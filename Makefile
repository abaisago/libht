# **************************************************************************** #
#                                         _____/\\\\\\\\\___                   #
#                                          ___/\\\///////\\\_                  #
#    Makefile                               __\/\\\_____\/\\\__                #
#                                            __\///\\\\\\\\\/___  |\/| /\ |\ | #
#    By: hachiman <adam_bai@protonmail.com>   ___/\\\///////\\\__ |  |/~~\| \| #
#                                              __/\\\______\//\\\_             #
#    Created: 2019/11/12 01:54:51 by hachiman   _\//\\\______/\\\__            #
#    Updated: 2020/01/21 18:21:48 by hachiman    __\///\\\\\\\\\/___           #
#                                                 ____\/////////______         #
# **************************************************************************** #

######################################################################
#                             VARIABLES                              #
######################################################################

NAME       := libht.a
PROJECT    := libht

CC         := gcc
CFLAGS     := -Wall -Wextra -Werror
CPPFLAGS   := -Iinclude

AR         := /usr/bin/ar
ARFLAGS    := rc
MAKE       := /usr/bin/make
RANLIB     := /usr/bin/ranlib

MKDIR      := /bin/mkdir
NORMINETTE := /usr/bin/norminette
PRINTF     := /usr/bin/printf
RM         := /bin/rm

#------------------------------------------------#
#                    SOURCES                     |
#------------------------------------------------#

SRC_UTIL   := ht_exit.c ht_argv_parser.c ht_itoa.c ht_shmem_new.c
SRC_UTIL   := $(addprefix util/,$(SRC_UTIL))

SRC_STRING := ht_string_append.c          ht_string_init.c           \
              ht_string_del.c             ht_string_resize.c         \
              ht_string_insert.c
SRC_STRING := $(addprefix string/,$(SRC_STRING))

SRC_ASSERT := ht_internal_fail.c               \
              ht_internal.c                    \
			  ht_internal_bin.c                \
			  ht_internal_floatequ.c           \
			  ht_internal_floatnequ.c          \
			  ht_internal_intequ.c             \
			  ht_internal_intnequ.c            \
              ht_internal_strequ.c             \
              ht_internal_strnequ.c
SRC_ASSERT := $(addprefix assert/,$(SRC_ASSERT))

SRC_UNIT   := ht_unit_new.c               ht_unit_run.c              \
              ht_unit_dot.c
SRC_UNIT   := $(addprefix unit/,$(SRC_UNIT))

SRC_SUITE  := ht_suite_dots.c              ht_suite_refresh.c        \
              ht_suite_messages.c          ht_suite_run.c            \
              ht_suite_new.c               ht_suite_summary.c
SRC_SUITE  := $(addprefix suite/,$(SRC_SUITE))

SRC_CAT    := ht_cat_new.c                ht_cat_refresh.c           \
              ht_cat_run.c                ht_cat_summary.c
SRC_CAT    := $(addprefix category/,$(SRC_CAT))

SRC_HT     := ht_add_unit.c               ht_add_cat.c               \
              ht_add_suite.c              ht_new.c                   \
              ht_refresh.c                ht_run.c                   \
              ht_summary.c                ht_won.c
SRC_HT     := $(addprefix ht/,$(SRC_HT))

SRC_NAME   := $(SRC_STRING)   \
              $(SRC_UTIL)     \
              $(SRC_ASSERT)   \
              $(SRC_UNIT)     \
              $(SRC_SUITE)    \
              $(SRC_CAT)      \
              $(SRC_HT)
SRC_PATH   := src
SRC        := $(addprefix $(SRC_PATH)/,$(SRC_NAME))

INC_PATH   := include

OBJ_PATH   := obj
OBJ_NAME   := $(SRC_NAME:.c=.o)
OBJ        := $(addprefix $(OBJ_PATH)/,$(OBJ_NAME))

#------------------------------------------------#
#                    RELEASE                     |
#------------------------------------------------#

REL_PATH   := release
NAME       := $(NAME)
REL_OBJ    := $(addprefix $(REL_PATH)/,$(OBJ))
REL_CFLAGS := $(CFLAGS)

#------------------------------------------------#
#                     DEBUG                      |
#------------------------------------------------#

DBG_PATH   := debug
DBG  	   := $(DBG_PATH)/$(NAME)
DBG_OBJ	   := $(addprefix $(DBG_PATH)/,$(OBJ))
DBG_CFLAGS := $(CFLAGS) -g

#------------------------------------------------#
#                     EXTRA                      |
#------------------------------------------------#

CLEAR      := "\033[0K\n\033[F"
CR         := "\r"$(CLEAR)
EOC        := "\033[0;0m"
RED        := "\033[0;31m"
GREEN      := "\033[0;32m"
YELLOW     := "\033[0;33m"



######################################################################
#                               RULES                                #
######################################################################
.PHONY: all, clean, fclean, re

#------------------------------------------------#
#                 RELEASE-RULES                  |
#------------------------------------------------#

all: $(NAME)

$(NAME): $(REL_OBJ)
	@$(PRINTF) $(CR)$(GREEN)"[ $(PROJECT): All object files have been created ]"$(EOC)"\n"
	@$(AR) $(ARFLAGS) $(NAME) $(REL_OBJ)
	@$(PRINTF) $(CR)$(GREEN)"[ $(PROJECT): $(NAME) has been created ]\n"$(EOC)

$(REL_PATH)/$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	@$(MKDIR) -p $(dir $@) 2>/dev/null || true
	@$(CC) $(REL_CFLAGS) $(CPPFLAGS) -c $< -o $@
	@$(PRINTF) $(CR)"[ $(PROJECT): %s ]"$(CLEAR) $@

#------------------------------------------------#
#                  DEBUG-RULES                   |
#------------------------------------------------#

dbg: $(DBG)

$(DBG): $(DBG_OBJ)
	@$(PRINTF) $(CR)$(GREEN)"[ $(PROJECT): All debug object files have been created ]"$(EOC)"\n"
	@$(AR) $(ARFLAGS) $(DBG) $(DBG_OBJ)
	@$(RANLIB) $(DBG)
	@$(PRINTF) $(CR)$(GREEN)"[ $(PROJECT): $(DBG) has been created ]\n"$(EOC)

$(DBG_PATH)/$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	@$(MKDIR) -p $(dir $@) 2>/dev/null || true
	@$(CC) $(DBG_CFLAGS) $(CPPFLAGS) -c $< -o $@
	@$(PRINTF) $(CR)"[ $(PROJECT): %s ]"$(CLEAR) $@

#------------------------------------------------#
#                  CLEAN-RULES                   |
#------------------------------------------------#

clean:
	@if [ -d $(REL_PATH)/$(OBJ_PATH) ]; then \
		$(RM) -f $(REL_OBJ) \
		&& $(RM) -rf $(REL_PATH)/$(OBJ_PATH) \
		&& $(PRINTF) $(CR)$(RED)"[ $(PROJECT): All object files cleaned ]\n"$(EOC); \
	fi

dclean:
	@if [ -d $(DBG_PATH)/$(OBJ_PATH) ]; then \
		$(RM) -f $(DBG_OBJ) \
		&& $(RM) -rf $(DBG_PATH)/$(OBJ_PATH) \
		&& $(PRINTF) $(CR)$(RED)"[ $(PROJECT): All debug object files cleaned ]\n"$(EOC); \
	fi

fclean: clean dclean
	@if [ -e $(NAME) ]; then \
		$(RM) -f $(NAME) \
		&& $(PRINTF) $(CR)$(RED)"[ $(PROJECT): $(NAME) cleaned ]\n"$(EOC); \
	fi
	@if [ -e $(DBG) ]; then \
		$(RM) $(DBG) \
		&& $(PRINTF) $(CR)$(RED)"[ $(PROJECT): $(DBG) cleaned ]\n"$(EOC); \
	fi
ifneq '$(REL_PATH)' '.'
	@$(RM) -rf $(REL_PATH)
endif	
ifneq '$(DBG_PATH)' '.'
	@$(RM) -rf $(DBG_PATH)
endif	

#------------------------------------------------#
#                  OTHER-RULES                   |
#------------------------------------------------#

soft: clean all

pure: all clean

re: fclean all

full: fclean all dbg

norme:
	$(NORMINETTE) $(SRC) $(INC_PATH)/*.h

FORCE:

.SILENT:
